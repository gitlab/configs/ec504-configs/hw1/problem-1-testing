package edu.bu.ec504.hw1p1;

import java.math.BigInteger;
import java.util.*;

public class Main {
    // constants
    static final int memSize = 10; // the number of bits backing the counter

    // ... test constants
    static final int DEFAULT_SIZE = 10; // number of bits in the default counter

    /**
     * Run {@link MyDistinctCounter} through the test strings provided, and report its results.
     * @param testStrings The strings to run by the {@link MyDistinctCounter}.
     * @param MAX_DIFF The maximum multiplicative error tolerance between
     *                 ctual and reported numbers of distinct strings.
     */
    static void test(ArrayList<String> testStrings, int MAX_DIFF) {
        Random rand = new Random(0); // fix the seed

        // Show the counter my test strings
        MyDistinctCounter tester = new MyDistinctCounter(memSize);
        Set<String> verifier = new HashSet<>();

        // initially add ~1/2 of the strings
        for (String test: testStrings) {
            if (rand.nextBoolean()) {
                tester.saw(test);
                verifier.add(test);
            }
        }

        // Record the counter's state
        FixedBitArray savedState = tester.currentState();

        // output current state and initialize a new counter
        MyDistinctCounter newTest = new MyDistinctCounter(memSize);
        newTest.setMem(savedState);

        // add all the strings to the new counter
        for (String test: testStrings) {
                tester.saw(test);
                verifier.add(test);
        }

        // Check that the answer is within accepted parameters
        int recorded = newTest.numDistinct();
        int actual = verifier.size();
        if ( ((float) recorded / actual > MAX_DIFF)
            || ((float) actual / recorded > MAX_DIFF) )
            throw new RuntimeException("Test failed - factor > " +
                    MAX_DIFF+
                    " between actual and guessed number of distinct entries.");
        System.out.println("... passed");
    }

    /**
     * Runs a simple test, based on the example in the homework description.
     */
    public static void runSimpleTest() {
        System.out.println("Simple test:");
        test(new ArrayList<>(List.of("Bravo", "Alfa", "Charlie", "Kilo", "Charlie", "Alfa", "Bravo")), 10);
    }

    /**
     * Runs a longer, more complicated test based on a supplied argument
     * @param arg The argument to use in the test.
     */
    public static void runLongerTest(String arg) {
        System.out.println("Longer test:");
        final BigInteger two = new BigInteger("2");
        final BigInteger modulus = new BigInteger(1,arg.getBytes()); // makes up a modulus based on the supplied argument
        BigInteger base = two;
        ArrayList<String> longList = new ArrayList<>(10000);
        for (int ii=0; ii<10000; ii++) {
            longList.add(base.toString());
            base=base.multiply(two).mod(modulus);
        }
        test(longList, 1000);
    }

    public static void main(String[] args) {
        // Method tests
        System.out.println("Method tests:");
        MyDistinctCounter test = new MyDistinctCounter(DEFAULT_SIZE);
        test.saw("This is only a test.");
        test.numDistinct();
        System.out.println("... passed");

        // System tests
        runSimpleTest();
        runLongerTest("Hello World");

        System.exit(0);
    }
}
