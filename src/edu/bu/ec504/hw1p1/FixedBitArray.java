package edu.bu.ec504.hw1p1;

import java.util.BitSet;

/**
 * Implements aan array of bits whose size cannot be changed.
 */
public class FixedBitArray {
  // FIELDS
  /**
   * The length, in bits, of this array.
   */
  public final int len;
  /**
   * The bits stored in this array.
   */
  final BitSet bits;

  // METHODS

  /**
   * Constructs an object that handles <code>theLen</code> bits.
   *
   * @param theLen The number of bits that the object can handle.
   */
  public FixedBitArray(int theLen) {
    len = theLen;
    bits = new BitSet(len);
  }

  public FixedBitArray(byte[] bytes) {
    len = bytes.length;
    bits = BitSet.valueOf(bytes);
  }

  /**
   * @param bitIndex The index at which the array is being queried.
   * @return The bit at the array index <code>bitIndex</code>.
   */
  public boolean get(int bitIndex) {
    confirmIndex(bitIndex);
    return bits.get(bitIndex);
  }

  /**
   * Sets the bit at index <code>bitIndex</code> to true.
   *
   * @param bitIndex The index to set true.
   */
  public void set(int bitIndex) {
    confirmIndex(bitIndex);
    bits.set(bitIndex);
  }

  /**
   * Clears the bit at index <code>bitIndex</code> to true.
   *
   * @param bitIndex The index to set true.
   */
  public void clear(int bitIndex) {
    confirmIndex(bitIndex);
    bits.clear(bitIndex);
  }

  /**
   * Flips the parity of the bit at index <code>bitIndex</code>
   *
   * @param bitIndex The index of the bit in the array to flip.
   */
  public void flip(int bitIndex) {
    confirmIndex(bitIndex);
    bits.flip(bitIndex);
  }

  /**
   * @return A byte array corresponding to memory of this object
   */
  public byte[] toByteArray() {
    rectifyBits();
    return bits.toByteArray();
  }

  /**
   * @param bytes The contents of the new array.
   * @return A FixedBitArray whose contents are <code>bytes</code>
   */
  static public FixedBitArray valueOf(byte[] bytes) {
    return new FixedBitArray(bytes);
  }

  @Override
  public String toString() {
    return "[" + len + " bits]: " + bits.toString();
  }

  /**
   * Confirms that <code>bitIndex</code> is within the dynamic range of indices
   * handled by this array, returning normally if confirmation succeeds.
   *
   * @param bitIndex The index to check.
   * @throws ArrayIndexOutOfBoundsException if <bode>bitIndex</bode> is out of
   * range for this array.
   */
  private void confirmIndex(int bitIndex) {
    if (bitIndex < 0 || bitIndex >= len) {throw new ArrayIndexOutOfBoundsException(bitIndex);}
  }

  /**
   * Make sure there are no bits outside the dynamic range of the array.
   */
  private void rectifyBits() {
    bits.set(len, bits.size(), false);
  }
}
